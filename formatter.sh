#!/bin/bash
TEMP=$(egrep -o "[^\/]+$" <<< "$2")
TEMP=$(egrep -o "[^0123456789]+" <<< "$TEMP")
if [ "$TEMP" = "sda" ]; then
	TEMP=$(egrep -o "[^\/]+$" <<< "$2")
	if [ "$1" != "u" ]; then
		sudo mkdir '/mnt/NTFS_MOUNT_'$TEMP && echo 'Created HDD mount directory. Mounting '$2'...'
		sudo mount -t ntfs-3g $2 '/mnt/NTFS_MOUNT_'$TEMP && echo 'Mounted.' && sudo mkdir '/mnt/NTFS_MOUNT_'$TEMP'/Users/Xona/TMP' && echo 'Created TMP folder.'
	else
		sudo rm -rf '/mnt/NTFS_MOUNT_'$TEMP'/Users/Xona/TMP' && echo 'Removed TMP folder.'
		sudo umount '/mnt/NTFS_MOUNT_'$TEMP && echo 'Unmounted.'
		sudo rm -rf '/mnt/NTFS_MOUNT_'$TEMP && echo 'Removed HDD mount directory.'
	fi
elif [ "$TEMP" = "sdc" ]; then
	if [ -z $4 ]; then
		FTYPE='ext4'
	else
		FTYPE=$4
	fi
	if [ -z $3 ]; then
		echo "No filsystem type provided as 3rd argument"
	else
		MNTYPE=$3
		if [ "$1" != "u" ]; then
			sudo mkdir /mnt/MOUNT_TMP && echo 'Created TMP and mount directories. Mounting USB...'
			sudo mount -t $MNTYPE $2 /mnt/MOUNT_TMP && echo 'Copying to TMP...' && cp -a /mnt/MOUNT_TMP /mnt/NTFS_MOUNT_sda3/Users/Xona/TMP && echo 'Copied files. Unmounting...' && sudo umount /mnt/MOUNT_TMP && echo 'Unmounted USB.'
		else
			sudo mkfs -t $FTYPE $2 && echo 'Formatted. Mounting USB...' && sudo mount -t $MNTYPE $2 /mnt/MOUNT_TMP && echo 'Mounted USB. Copying from TMP...' && cp -a /mnt/NTFS_MOUNT_sda3/Users/Xona/TMP /mnt/MOUNT_TMP && echo 'Copied files. Unmounting...' && sudo umount /mnt/MOUNT_TMP && sudo rm -rf /mnt/MOUNT_TMP && echo 'Unmounted USB, removed mount folder.'
		fi
	fi
else
	$MNTYPE=""
	$FTYPE="ext4"
fi
fdisk -l | grep $TEMP
ls -al /mnt