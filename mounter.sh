if [ $1 != 'u' ]
then
    if [ '{$2}' = 'NTFS' ]; then
        sudo mkdir '/mnt/NTFS_MOUNT_POINT_'$2 && echo "Created mount directory." && sudo mount -t ntfs-3g $1 '/mnt/NTFS_MOUNT_POINT_'$2 && echo "Mounted drive. Completed."
    elif [ '{$2}' = 'FAT32' ]; then
        sudo mkdir '/mnt/NTFS_MOUNT_POINT_'$2 && echo "Created mount directory." && sudo mount -t vfat $1 '/mnt/NTFS_MOUNT_POINT_'$2 && echo "Mounted drive. Completed."
    elif [ '{$2}' = 'EXFAT' ]; then
        sudo mkdir '/mnt/NTFS_MOUNT_POINT_'$2 && echo "Created mount directory." && sudo mount -t vfat $1 '/mnt/NTFS_MOUNT_POINT_'$2 && echo "Mounted drive. Completed."
    else
        sudo mkdir '/mnt/NTFS_MOUNT_POINT_'$2 && echo "Created mount directory." && sudo mount $1 '/mnt/NTFS_MOUNT_POINT_'$2 && echo "Mounted drive. Completed."
    fi
else
    if [ $2 -eq -1 ]; then
        sudo umount '/mnt/NTFS_MOUNT_POINT' && echo "Unmounted drive." && sudo rm -rf '/mnt/NTFS_MOUNT_POINT' && echo "Removed mount directory. Completed"
    elif [ $2 -eq 0 ]; then
        sudo umount '/mnt/NTFS_MOUNT_POINT'$2 && echo "Unmounted drive." && sudo rm -rf '/mnt/NTFS_MOUNT_POINT'$2 && echo "Removed mount directory. Completed"
    else
        sudo umount '/mnt/NTFS_MOUNT_POINT_'$2 && echo "Unmounted drive." && sudo rm -rf '/mnt/NTFS_MOUNT_POINT_'$2 && echo "Removed mount directory. Completed"
    fi
fi